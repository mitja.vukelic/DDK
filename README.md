- [1. Kasa](#1-kasa)
  - [Izrada kase](#izrada-kase)
  - [Izrada kase](#izrada-kase-1)
  - [Migracija](#migracija)
  - [Finaliziranje](#finaliziranje)
- [2. Virga](#2-virga)
  - [Općenito](#općenito)
  - [Parametri kase](#parametri-kase)
  - [Maloprodaja](#maloprodaja)
  - [Robno](#robno)
- [3. Cheet sheet](#3-cheet-sheet)
  - [Virga cheat sheet](#virga-cheat-sheet)

# 1. Kasa
## Izrada kase

**jPos Packer se nalazi u folderu
L:\Prod\Office AP\bruno\jPosPacker**
1. Odabrati platformu ( Virga ili Intersport)
2. Odabrati firmu za koju se definira kasa
3. Odabrati folder gdje će se generirati kasa
4. Odabrati trace level DEBUG ili INFO
5. Odabrati firmu za koju se definira kasa
6. Odabrati broj kase
7. Odabrati vrstu kase
8. Odabrati način pripreme kase:
   1. posIniFile - generira samo ini file
   2.  jPos file - generira samo .jar file
   3.  Windows instalacija
   4.  Windows zip
   5.  Linux zip
9. Ograničiti Java Heap memoriju ako je potrebno
10. Odabrati vrstu pisača:
    1.  RXTX
    2.  SUN
11. Po potrebi odabrati verziju kase
12. Pritisnuti button "Pripremi init"
13. Pritisnuti button "Započni"
14. U odabranom folderu generirat će se kasa zavisno od načina pripreme.
    Ukoliko je odabran windows zip kreirat će se "Firma_Organizacija_kasa_jpos.zip".
    Primjer - Harissa u Rijeci kasa 1 je "Harissa_MP07_01_jpos.zip" 
## Izrada kase
**ToDo**
## Migracija
**ToDo**
## Finaliziranje
**ToDo**

# 2. Virga
## Općenito
**ToDo**
## Parametri kase
## Maloprodaja
**ToDo**
## Robno
**ToDo**

# 3. Cheet sheet
## Virga cheat sheet
| Akcija                     | Kratica            |
| -------------------------- | ------------------ |
| Spremi sve (Potvrda)       | F10                |
| Izbriši sve (Očisti formu) | Shift + F7         |
| Otkaži akciju              | Esc                |
| Očisti blok                | Shift + F5         |
| Očisti polje               | Ctrl + u           |
| Očisti slog                | Shift + F4         |
| Prebroji odg slogove       | Shift +F2          |
| Briši natrag               | Backspace          |
| Briši slog                 | Shift + F6         |
| Pokaži pogrešku            | Shift + F1         |
| Dolje                      | Ctrl + i ili ↓     |
| Dupliciraj polje           | F3                 |
| Dupliciraj slog            | F4                 |
| Editiraj                   | Ctrl + e           |
| Postavi upit               | F7                 |
| Izvrši upit                | F8                 |
| Prekini upit               | Ctrl + q           |
| Izlaz                      | Ctrl + q           |
| Pomoć                      | F1                 |
| Glavni meni                | Ctrl + .           |
| Novi slog                  | F6                 |
| Slijedeći blok             | Ctrl + page down   |
| Slijedeće polje            | Tab ili Ctrl + Tab |
| Slijedeći primarni ključ   | Shift + F3         |
| Slijedeći slog             | Shift + →          |
| Slijedeći skup slogova     | Ctrl + >           |
| Prethodni blok             | Ctrl + Page up     |
| Prethodno polje            | Shift + Tab        |
| Prethodni slog             | Shift + ←          |
| Print                      | Shift + F8         |
| Lista vrijednosti          | F9                 |
